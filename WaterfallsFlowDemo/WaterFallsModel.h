//
//  WaterFallsModel.h
//  WaterfallsFlowDemo
//
//  Created by 伟徐 on 2022/3/7.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface WaterFallsModel : NSObject
@property(nonatomic,copy)NSString *imageUrl;
@property (nonatomic,assign)CGFloat imageHeight;

@property(nonatomic,copy)NSString *title;
@property(nonatomic,assign)CGFloat titleHeight;

@property(nonatomic,copy)NSString *price;
@property(nonatomic,assign)CGFloat priceHeight;
@property(nonatomic,assign)CGFloat priceWidth;

@property(nonatomic,copy)NSString *saleNum;
@property(nonatomic,assign)CGFloat saleWidth;

@end

NS_ASSUME_NONNULL_END
