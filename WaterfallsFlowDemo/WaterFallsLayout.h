//
//  WaterFallsLayout.h
//  WaterfallsFlowDemo
//
//  Created by CyanXu on 2022/3/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol WaterFallsLayoutDelegate <NSObject>

- (CGFloat)collectionView:(UICollectionView *)collectionView heightForItemAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface WaterFallsLayout : UICollectionViewLayout
@property(nonatomic,assign)NSInteger numberOfColumns;
@property(nonatomic,assign)CGFloat minimumLineSpacing;
@property(nonatomic,assign)CGFloat minimumInteritemSpacing;
//@property(nonatomic,assign)CGSize collectionViewContentSize;
@property(nonatomic,assign)CGFloat width;

@property(nonatomic,assign)id<WaterFallsLayoutDelegate> delegate;
@property(nonatomic,strong)NSMutableArray<UICollectionViewLayoutAttributes *> *cache;
@property(nonatomic,assign)CGFloat contentHeight;

@end

NS_ASSUME_NONNULL_END
