//
//  WaterFallsCell.m
//  WaterfallsFlowDemo
//
//  Created by 伟徐 on 2022/3/7.
//

#import "WaterFallsCell.h"
#import <Masonry/Masonry.h>
@interface WaterFallsCell()
@property (nonatomic,strong)UILabel *titleLabel;//标题
@property (nonatomic,strong)UILabel *praceLabel;//价格
@property (nonatomic,strong)UILabel *msaleLabel;//销量
@property (nonatomic,strong)UILabel *noteLabel;//备注
@end

@implementation WaterFallsCell

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self initWithUIFrame:frame];
    }
    return self;
}

- (void)initWithUIFrame:(CGRect)rect
{
    self.imageView = [[UIImageView alloc] init];
//    _imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
//    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
//    self.imageView.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:self.imageView];
//    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.left.right.mas_equalTo(self.contentView);
////        make.height.mas_equalTo(100);
//    }];

    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    self.titleLabel.numberOfLines = 0;
    [self.titleLabel sizeToFit];
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.imageView.mas_bottom).offset(0);
        make.left.right.equalTo(self.contentView);
//        make.height.mas_equalTo(20);
    }];

    self.praceLabel = [[UILabel alloc]init];
    self.praceLabel.font = [UIFont systemFontOfSize:16.0f];
    self.praceLabel.numberOfLines = 0;
//    self.praceLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.praceLabel sizeToFit];
    [self.contentView addSubview:self.praceLabel];

    
    self.msaleLabel = [[UILabel alloc]init];
    self.msaleLabel.font = [UIFont systemFontOfSize:13.0f];
    self.msaleLabel.numberOfLines = 0;
//    self.msaleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.msaleLabel sizeToFit];
    [self.contentView addSubview:self.msaleLabel];
}

//- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes{
//
//    [self setNeedsLayout];
//    [self layoutIfNeeded];
//    CGSize size = [self.contentView systemLayoutSizeFittingSize: layoutAttributes.size];
//    CGRect cellFrame = layoutAttributes.frame;
//    cellFrame.size.height= size.height;
//    layoutAttributes.frame= cellFrame;
//    return layoutAttributes;
//}

- (void)setModel:(WaterFallsModel *)model{
//    self.model = model;
    self.titleLabel.text = model.title;
    self.praceLabel.text = model.price;
    self.msaleLabel.text = model.saleNum;
    
    NSLog(@"Image的内存地址:%p",self.imageView);
    NSLog(@"%p model.imageHeight=%.1f",model,model.imageHeight);

    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(model.imageHeight);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.imageView.mas_bottom).offset(0);
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(model.titleHeight);
    }];

    [self.praceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).mas_offset(5);
        make.left.equalTo(self.contentView).mas_offset(10);
        make.width.mas_equalTo(model.priceWidth);
        make.height.mas_equalTo(model.priceHeight);
    }];

    [self.msaleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).mas_offset(7);
        make.left.mas_equalTo(self.praceLabel.mas_right).offset(5);
        make.width.mas_equalTo(model.saleWidth);
        make.height.mas_equalTo(model.priceHeight);
    }];
}


@end
