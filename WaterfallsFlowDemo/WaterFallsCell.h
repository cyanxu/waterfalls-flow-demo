//
//  WaterFallsCell.h
//  WaterfallsFlowDemo
//
//  Created by 伟徐 on 2022/3/7.
//

#import <UIKit/UIKit.h>
#import "WaterFallsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface WaterFallsCell : UICollectionViewCell
@property (nonatomic,strong) WaterFallsModel *model;
@property (nonatomic,strong)UIImageView *imageView;

@end

NS_ASSUME_NONNULL_END
